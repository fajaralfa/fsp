import express from 'express'
import process from 'node:process'

import { User } from './database.js'
import { jsonParse } from './helper.js'

const api = express.Router()
const web = express.Router()

web.get('/*', (req, res) => {
  const data = {
    env: process.env.NODE_ENV,
    manifest: jsonParse('public/manifest.json'),
  }
  res.render('index.html', data)
})

api.get('/', (req, res) => {
  res.send('wa')
})

api.post('/login', (req, res) => {
  res.send('w')
})

api.post('/register', (req, res) => {
  console.log(req.body)
  res.json({})
})

export default { api, web }
