import fs from 'node:fs/promises'
import crypto from 'node:crypto'

//@type-check

export async function jsonParse(file) {
  const content = await fs.readFile(file)
  return JSON.parse(content)
}

export const pbkdf2 = {
  config: {
    iteration: 100000,
    keylen: 64,
    digest: 'sha512',
  },

  createHash: async function (password, salt) {
    return new Promise((resolve, reject) => {
      crypto.pbkdf2(
        password,
        salt,
        this.config.iteration,
        this.config.keylen,
        this.config.digest,
        (err, derivedKey) => {
          if (err) return reject(err)
          const hash = derivedKey.toString('hex')
          const combinedHash = [salt, hash].join('$')
          resolve(combinedHash)
        }
      )
    })
  },

  pwHash: async function (password) {
    const salt = crypto.randomBytes(16).toString('hex')
    return await this.createHash(password, salt)
  },

  verify: async function (password, combinedHash) {
    const [salt] = combinedHash.split('$')
    const inputHash = await this.createHash(password, salt)
    return inputHash === combinedHash
  },
}

export const sha512 = {
  createHash: function (password, salt) {
    const hash = crypto.createHash('sha512')
    hash.update([password, salt].join())

    const combinedHash = [salt, hash.digest('hex')].join('$')
    return combinedHash
  },

  pwHash: function (password) {
    const salt = crypto.randomBytes(16).toString('hex')
    const combinedHash = this.createHash(password, salt)
    return combinedHash
  },

  verify: function (password, combinedHash) {
    const [salt] = combinedHash.split('$')

    const inputHash = this.createHash(password, salt)
    return inputHash === combinedHash
  },
}
