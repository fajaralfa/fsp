//@type-check

import { Server } from "socket.io";

/**
 * @param {Server} io 
 * */
export default function socketEvent(io) {

  io.on('connection', (socket) => {
    console.log('user connected')
    io.on('disconnect', (i) => {
      console.log('user disconnect')
    })
  })

}
