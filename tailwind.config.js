import daisyui from 'daisyui'

/** @type {import('tailwindcss').Config} */
export default {
  content: ['./src/index.html', './src/frontend/**/*.jsx'],
  theme: {
    extend: {},
  },
  plugins: [daisyui],
}
