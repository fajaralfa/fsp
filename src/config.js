import dotenv from 'dotenv'

const conf = dotenv.config({
  path: process.env.NODE_ENV === 'production' ? '.env' : '.env.dev',
}).parsed

export default conf
