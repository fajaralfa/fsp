import app from './src/app.js'

app.listen(process.env.PORT || 3000, () => {
  console.log('NODE_ENV ', process.env.NODE_ENV)
  console.log('server listen on port', 3000)
})
