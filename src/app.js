import express from 'express'
import { createServer } from 'node:http'
import { Liquid } from 'liquidjs'
import { Server } from 'socket.io'

import router from './router.js'
import socketEvent from './socket-event.js'

const app = express()
const server = createServer(app)
const io = new Server(server)

app.engine('html', new Liquid().express())
app.set('views', 'src')

app.use(express.static('public'))
app.use('/api', router.api)
app.use('/', router.web)

socketEvent(io)

export default server
