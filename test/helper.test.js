import { expect, test, describe } from 'vitest'
import { pbkdf2, sha512 } from '../src/helper.js'

describe('pbkdf2 password hashing test', () => {
  test('pbkdf2 pwHash', async () => {
    const actual = await pbkdf2.pwHash('kata sandi')
    expect(actual).toContain('$')
  })

  test('pbkdf2 verify success', async () => {
    const savedHash = await pbkdf2.pwHash('rahasia')
    const result = await pbkdf2.verify('rahasia', savedHash)

    expect(savedHash).toContain('$')
    expect(result).toBe(true)
  })

  test('pbkdf2 verify failed not same', async () => {
    const savedHash = await pbkdf2.pwHash('rahasia')
    const result = await pbkdf2.verify('salah', savedHash)

    expect(savedHash).toContain('$')
    expect(result).toBe(false)
  })

  test('pbkdf2 verify failed error', async () => {
    const savedHash = await pbkdf2.pwHash('')
    const result = await pbkdf2.verify('salah', savedHash)

    expect(savedHash).toContain('$')
    expect(result).toBe(false)
  })
})


describe('sha512 password hashing test', () => {
  test('sha512 pwHash', () => {
    const actual = sha512.pwHash('kata sandi')
    expect(actual).toContain('$')
  })

  test('sha512 verify success', () => {
    const savedHash = sha512.pwHash('rahasia')
    const result = sha512.verify('rahasia', savedHash)

    expect(savedHash).toContain('$')
    expect(result).toBe(true)
  })

  test('sha512 verify failed not same', () => {
    const savedHash = sha512.pwHash('rahasia')
    const result = sha512.verify('salah', savedHash)

    expect(savedHash).toContain('$')
    expect(result).toBe(false)
  })

  test('sha512 verify failed error', () => {
    const savedHash = sha512.pwHash('')
    const result = sha512.verify('salah', savedHash)

    expect(savedHash).toContain('$')
    expect(result).toBe(false)
  })
})
