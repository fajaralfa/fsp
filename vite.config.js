import { fileURLToPath, URL } from 'url'
import { defineConfig } from 'vite'
import solidPlugin from 'vite-plugin-solid'

export default defineConfig({
  plugins: [solidPlugin()],
  build: {
    manifest: true,
    rollupOptions: {
      input: 'src/frontend/main.jsx',
    },
    outDir: 'public',
  },
  publicDir: false,
})
