import { DataTypes, Sequelize } from 'sequelize'
import conf from './config.js'

const sequelize = new Sequelize({
  dialect: 'mysql',
  host: conf.DB_HOST,
  username: conf.DB_USERNAME,
  password: conf.DB_PASS,
  database: conf.DB_NAME,
})

export const User = sequelize.define('User', {
  name: {
    type: DataTypes.STRING,
    allowNull: false,
  },
  username: {
    type: DataTypes.STRING,
    unique: true,
    allowNull: false,
  },
  role: {
    type: DataTypes.ENUM('sender', 'receiver'),
    allowNull: false,
  },
})

export const Answer = sequelize.define('Answer', {
  answer: {
    type: DataTypes.ENUM('yes', 'no'),
    allowNull: false,
  },
})

export default {
  User,
}
