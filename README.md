fjr side project

# REST API

## User Register

### Request

Endpoint : POST /api/users  

Body :

```json
{
    "name" : "User Name",
    "username" : "username",
    "password" : "secret"
}
```

### Response Sucess

Status : 201

Body :

```json
{
    "data" : {
        "name" : "User Name",
        "password" : "secret"
    }
}
```

### Response Error Field

Status : 400

Body :

```json
{
    "messages" : {
        "name" : "Name cannot be empty",
        "name" : "Name cannot contain numbers and symbols",
        "username" : "Username cannot be less than 3 and more than 20 characters",
        "password" : "Password cannot be less than 6 characters"
    }
}
```

## User Login

### Request

Endpoint : POST /api/users/login

Body :

```json
{
    "username" : "username",
    "password" : "secret"
}
```

### Response Success

Status : 200

Body : 

```json
{
    "token" : "jsonwebtoken"
}
```

### Response Error Wrong Credential

Status : 401

Body :

```json
{
    "message" : "username or password is wrong"
```

## Dashboard

get the user info

### Request

Endpoint : GET /api/users

Headers :

```json
{
    "Authorization" : "Bearer jsonwebtoken"
}
```

### Response Success

status : 200

Body :

```json
{
    "name" : "Your Name",
    "username" : "username"
}
```

### Response Error Invalid Token

Status : 401

Body : 

```json
{
    "message" : "Invalid Token"
}
```
